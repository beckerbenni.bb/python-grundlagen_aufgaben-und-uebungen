""" 
Schreiben Sie ein Programm, das zwei Zahlen den Vorgaben entsprechend verrechnet. 
Das Programm soll zu Beginn dem Nutzer eine Auswahl mit den verschiedenen Möglichkeiten anbieten.

Bsp. Auswahl: 
(1) Addition
(2) Subtraktion
(3) Multiplikation
(4) Division

Im Anschluss:
Erweitere deine Lösung, indem du für die Berechnung 4 verschiedene Funktionen definierst. 
Passe ausserdem dein Programm so an, dass der Benutzer mehrere Berechnungen nacheinander durchführen kann und den Zeitpunkt zum Beenden des Programms selber bestimmt.
"""