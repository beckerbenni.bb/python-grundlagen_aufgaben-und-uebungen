"""
Kein Mitspieler vorhanden? Dann programmieren Sie einfach ihren eigenen Spielpartner.

Zuerst trifft der nutzer eine Entscheidung (Schere, Stein oder Papier) und dann trifft der Computer seine. Damit der Nutzer seinen Zug machen kann, können Sie entweder die Enscheidungen durch Auswahlbuchstaben vorgeben oder den Nutzer das Wort (Schere, Stein oder Papier) eintippen lassen.

Am Ende wird auf Basis der beiden Entscheidungen (Nutzer, Computer) ein Gewinner der Runde ermittelt.
Sie können dann entweder die Möglichkeit geben erneut zu spielen oder den Nutzer im Voraus eine Anzahl von Runden festgelegte lassen. Es muss natürlich eine Funktion zur Punkteverfolgung eingerichtet werden, die am Ende den Sieger ermittelt.
"""