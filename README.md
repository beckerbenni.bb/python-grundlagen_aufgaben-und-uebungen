## Python-Grundlagen-Aufgaben

## Geförderte Programmierkonzepte:

### Würfelgenerator:
* Variablen
* Random-Funktion

### Temperatur Umwandler:
* Variablen
* Verzweigungen
* Verarbeitung von Nutzereingaben

### Passwortgenerator
* Variablen
* Verarbeitung von Nutzereingaben
* Weitere Ausbaustufen möglich ...

### Textbasiertes Abenteuerspiel
* Variablen
* Verzweigungen
* Verarbeitung von Nutzereingaben
* **beliebig ausbaubar**

### sum_numbers:
* Variablen
* Strings
* Verzweigung
* Schleifen
* Funktionen nutzen

### palindrom:
* Variablen
* Strings
* Verzweigung
* (Schleifen)
* Funktionen nutzen

### Schere, Stein, Papier
* Variablen
* Listen
* Random-Funktion
* Verzweigung
* Schleifen

### Taschenrechner
* Variablen
* Verarbeitung von Nutzereingaben
* Verzweigung
* Funktionen
* (Schleifen)

### Zahlenraten
* Variablen
* Verarbeitung von Nutzereingaben
* Verzweigung
* Schleifen
* Funktionen
