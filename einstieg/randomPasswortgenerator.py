"""
Ein sicheres Passwort zu erstellen und sich daran zu erinnern, ist keine leichte Aufgabe. Erstellen Sie ein Programm, das einige Wörter des Benutzers aufnimmt und dann aus diesen Wörtern ein zufälliges Passwort generiert. Der Benutzer kann sich anschließend, mit Hilfe der Wörter die er als Eingabe eingegeben hat, an das Passwort erinnern.
"""