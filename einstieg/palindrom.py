# Ein Wort soll dahingehend überprüft werden, ob es sich um ein Palindrom handelt oder nicht. 

# palindromcheck: Input: ein string (Zeichenfolge) / Output: ein boolean (Wahrheitswert)
def palindromcheck(word: str) -> bool:
    
    # TODO: Hier ist der Platz für deinen Code
    
    return False

if __name__ == '__main__':
    assert palindromcheck('Hallo') == False
    assert palindromcheck('neben') == True
    assert palindromcheck('REgalLaGer') == True
    assert palindromcheck('Hannah') == True
    assert palindromcheck('Rababer') == False
    print("Glückwunsch, alle Tests wurden bestanden.")
    
    